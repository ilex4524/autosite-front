import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import 'normalize.css'

import Home from '../views/Home'
import Contacts from '../views/Contacts'
import Feedback from '../views/Feedback'
import Admin from '../views/Admin'


Vue.use(Router)
Vue.use(ElementUI)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/contacts',
      component: Contacts
    },
    {
      path: '/feedback',
      component: Feedback,
    },
    {
      path: '/admin',
      component: Admin
    }
  ]
})
