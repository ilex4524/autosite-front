import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex)

const store = new Vuex.Store({
    state:{
      admin:{
        auth: false,
        token: '',

        page: 'qiwi',
        expendedQiwi:{
          'qw':{
            _account: '2'
          }
        },

        //apiLink: `http://${document.location.hostname}:3000/`,
        apiLink: `http://185.86.79.79:3000/`
      },
    },
    mutations:{
      setAdminPage(state, page){
        state.admin.page = page;
      },
      // setWalletInfo(state, data){
      //   state.admin.walletInfo = data;
      // }
    },
    actions: {
      postRequest({commit}, {link, data, callback, onError=()=>{}}) {
        axios.post(link, data)
        .then(res => {callback(res.data)})
        .catch(err => {console.log('postRequest', err); onError(); })
      },
      getTextContent({commit}, {callback}){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + `settings/get_content`,
            data: {},
            callback: (res) => { console.log(res); callback(res);  }
          });
      },      
      calculateFee({commit}, {amount, callback}) {
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + `exchange/fee`,
            data: {amount},
            callback: (res) => { callback(res) }
          });
      },
      sendForm({commit}, {amount, account, email, callback, onError }) {
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + `exchange/request`,
            data: { amount, account, email },
            callback: (res) => {
              alert(res)
              if (res.success) callback(res.data)
            },
            onError
          });
      },
      exchangePaid({commit}, {order, callback, onError }) {
        
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + `exchange/paid`,
            data: { order },
            onError: ()=>{
              onError()
            },
            callback: (res) => {
              if (res.accepted && res.success) callback(res)
                else onError()
            }
          });
      },
      addComment({commit}, {name, text, email, callback }){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + `comments/add_comment`,
            data: { name, text, email },
            callback: (res) => { callback(res) }
          });
      },
      getCommentsList({commit}, {page, callback}){
        store.dispatch('postRequest',
        {
          link: store.state.admin.apiLink + 'comments/comments',
          data: {offset: page},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
              else alert('server error')
          }
        });
      },
      getLastCommentsList({commit}, {page, callback}){
        store.dispatch('postRequest',
        {
          link: store.state.admin.apiLink + 'comments/last_comments',
          data: {},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
              else alert('server error')
          }
        });
      },
      getCommentsPages({commit}, {callback}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'comments/comments_pages',
          data: {},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
              else alert('server error')
          }
        });
      },
      adminSignIn({commit}, {login, pass}){
        
        store.dispatch('postRequest',
        {
          link: store.state.admin.apiLink + 'auth/login',
          data: {
            login: login,
            password: pass
          },
          callback: (res) => {
            // localStorage.setItem('token', res.data.token);
            // location.reload(true);
            if (res.accepted && res.success) {
              store.state.admin.auth = true;
              store.state.admin.token = res.data.token;
            } else alert('server error')
          }
        });

      },
      addAdminQiwi({commit}, {account, token, callback}){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + 'settings/set_wallet',
            data: { jwt: store.state.admin.token, account, token },
            callback: (res) => {
              if (res.accepted && res.success) callback();
               else alert('server error')
            }
        });
      },
      addAdminWex({commit}, {key, secret, callback}){
          store.dispatch('postRequest',
            {
              link: store.state.admin.apiLink + 'settings/set_wex',
              data: { jwt: store.state.admin.token, key:key, secret },
              callback: (res) => {
                if (res.accepted && res.success) {
                  console.log(callback);
                  
                  callback();
                } else alert('server error')
              }
          });
      },
      getAdminQiwi({commit}, {callback}){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + 'settings/get_wallets',
            data: {jwt: store.state.admin.token},
            callback: (res) => {
              if (res.accepted && res.success) {
                store.state.admin.qiwi = res.data;
                for (let i in res.data)
                  store.state.admin.expendedQiwi[res.data[i]._account] = {}
                callback(res.data)
              } else alert('server error')
            }
          });
      },
      getAdminWex({commit}, {callback}){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + 'settings/get_wex',
            data: {jwt: store.state.admin.token},
            callback: (res) => {
              if (res.accepted && res.success) {
                callback(res.data)
              } else alert('server error')
            }
          });
      },
      getAdminQiwiInfo({commit}, {id, callback}){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + 'settings/get_wallet',
            data: {
              jwt: store.state.admin.token,
              id: id,
            },
            callback: (res) => {
              if (res.accepted && res.success) {
                console.log(res);
                callback( res.data[0] )
              } else alert('server error')
            }
          });
      },
      removeAdminQiwi({commit}, {id, callback}){
        store.dispatch('postRequest',
          {
            link: store.state.admin.apiLink + 'settings/remove_wallet',
            data: {
              jwt: store.state.admin.token,
              id: id,
            },
            callback: (res) => {
              if (res.accepted && res.success)  callback(  ) 
               else alert('server error')
            }
          });
      },
      getAdminComments({commit}, {callback}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'comments/admin_comments',
            data: {jwt: store.state.admin.token},
            callback: (res) => {
              if (res.accepted && res.success) {
                callback(res.data)
              } else alert('server error')
            }
          });
      },

      acceptAdminComment({commit}, {id, callback}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'comments/accept_comment',
            data: {jwt: store.state.admin.token, comment: id},
            callback: (res) => {
              if (res.accepted && res.success) {
                callback()
              } else alert('server error')
            }
          });
      },
      deleteAdminComment({commit}, {id, callback}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'comments/delete_comment',
            data: {jwt: store.state.admin.token, comment: id},
            callback: (res) => {
              if (res.accepted && res.success) {
                callback()
              } else alert('server error')
            }
          });
      },

      getAdminOrders({commit}, {callback}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'orders/all',
            data: {jwt: store.state.admin.token},
            callback: (res) => {
              if (res.accepted && res.success) {
                callback(res.data)
              } else alert('server error')
            }
          });
      },
      getFee({commit}, {callback}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/get_fee',
          data: {jwt: store.state.admin.token},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
             else alert('server error')
          }
        });
      },
      getLimit({commit}, {callback}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/get_limit',
          data: {jwt: store.state.admin.token},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data) 
            else alert('server error')
          }
        });
      },
      getLetters({commit}, {callback}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/get_letters',
          data: {jwt: store.state.admin.token},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
             else alert('server error')
          }
        });
      },
      getContacts({commit}, {callback}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/get_contacts',
          data: {jwt: store.state.admin.token},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
             else alert('server error')
          }
        });
      },
      addStatus({commit}, {callback, name}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/add_status',
          data: {jwt: store.state.admin.token, name},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
             else alert('server error')
          }
        });
      },
      getStatus({commit}, {callback}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/get_status',
          data: {jwt: store.state.admin.token},
          callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
             else alert('server error')
          }
        });
      },
      updateFee({commit}, {callback, fee}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/update_fee',
          data: {jwt: store.state.admin.token, fee},
          callback: (res) => {
            if (res.accepted && res.success) {
              callback(res.data)
            } else alert('server error')
          }
        });
      },
      updateLimit({commit}, {callback, limit}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/update_limit',
            data: {jwt: store.state.admin.token, limit},
            callback: (res) => {
              if (res.accepted && res.success) {
                callback(res.data)
              } else alert('server error')
            }
          });
      },
      updateCouponLetters({commit}, {callback, subject, text}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/update_coupon_letter',
          data: {jwt: store.state.admin.token, subject, text},
          callback: (res) => {
            if (res.accepted && res.success) {
              callback(res.data)
            } else alert('server error')
          }
        });
      },
      updatePayoutLetters({commit}, {callback, subject, text}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/update_order_letter',
            data: {jwt: store.state.admin.token, subject, text},
            callback: (res) => {
            if (res.accepted && res.success) callback(res.data)
               else alert('server error')
            }
          });
      },
      addContact({commit}, {callback, name, value}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/add_contact',
            data: {jwt: store.state.admin.token, name, value},
            callback: (res) => {
              if (res.accepted && res.success) callback(res.data)
               else alert('server error')
            }
          });
      },
      updateContact({commit}, {callback, name, value}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/update_contact',
            data: {jwt: store.state.admin.token, name, value},
            callback: (res) => {
              if (res.accepted && res.success) callback(res.data)
               else alert('server error')
            }
          });
      },
      removeContact({commit}, {callback, name}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/remove_contact',
            data: {jwt: store.state.admin.token, name},
            callback: (res) => {
              if (res.accepted && res.success) callback(res.data)
               else alert('server error')
            }
          });
      },
      setContentHomeMain({commit}, {callback, subject, text}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/set_content_home_main',
            data: {jwt: store.state.admin.token, subject, text},
            callback: (res) => {
              if (res.accepted && res.success) callback(res); 
               else alert('server error')
            }
          });
      },
      setContentHomeMain2({commit}, {callback, subject, text}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/set_content_exchange',
            data: {jwt: store.state.admin.token, subject, text},
            callback: (res) => {
              if (res.accepted && res.success) callback(res); 
               else alert('server error')
            }
          });
      },
      setContentFeedbackLinks({commit}, {callback, subject, text}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/set_content_feedback',
          data: {jwt: store.state.admin.token, subject, text},
          callback: (res) => {
            if (res.accepted && res.success) callback(res); 
              else alert('server error')
          }
        });
      },
      setCheker({commit}, {callback, id}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/set_checker',
          data: {jwt: store.state.admin.token, id},
          callback: (res) => {
            if (res.accepted && res.success) callback(res); 
             else alert('server error')
          }
        });
      },
      PayFormSubmit({commit}, {callback,onError, wallet, amount, account}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/withdraw',
          data: {jwt: store.state.admin.token, wallet, amount, account},
          callback: (res) => {
            if (res.accepted && res.success) callback(res); 
             else alert('server error')
          }, onError
        });
      },
      updateQiwiToken({commit}, {callback, wallet, token}){
        store.dispatch('postRequest', {
          link: store.state.admin.apiLink + 'settings/update_qiwi_token',
          data: {jwt: store.state.admin.token, wallet, token},
          callback: (res) => {
            if (res.accepted && res.success) callback(); 
             else alert('server error')
          }
        });
      },
      updateRules({commit}, {callback, text}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/set_content_rules',
            data: {jwt: store.state.admin.token, text},
            callback: (res) => {
              if (res.accepted && res.success) callback()
               else alert('server error')
            }
          });
      },
      updatePass({commit}, {callback, old, newp}){
        store.dispatch('postRequest', {
            link: store.state.admin.apiLink + 'settings/reset_password',
            data: {jwt: store.state.admin.token, old, new: newp },
            callback: (res) => {
              if (res.accepted && res.success) callback()
               else alert('server error')
            }
          });
      },
    }
})

export default store
